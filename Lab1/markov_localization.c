#include <stdio.h>

#define nb_state 20
#define nb_action 1
#define nb_observation 2
#define wall 0
#define door 1

float current_localization[nb_state]; // used to store the current probability distribution over the states
float previous_localization[nb_state]; // used to store the previous probability distribution over the states
float dynamic_tab[nb_state+1][nb_action+1];
float sensor_tab[nb_observation][nb_observation];
int map_left[nb_state];
int map_right[nb_state];

void init_position(int position) {
// if position == -1 then unknown initial position
// otherwise the initial position is "position"

	int i, current_state;

	if(position == -1)
	{		
		current_state = 1 / (float) nb_state;

		for(i = 0; i < nb_state; i++){
			current_localization[i] = current_state;
		}
	}else
	{		
		current_state = position;
		current_localization[current_state] = 1;
		for(i = 0; i < nb_state; i++){
			current_localization[i] = 0;
		}
	}
}// init_position

float dynamic_model(int current_state, int previous_state, int action) {
//return the probability to reach the state "current_state" from the state "previous_state" performing the action "action"

	int loop, loop2;

	for( loop = 0; loop < nb_state; loop++ )
		for( loop2 = 0; loop2 < nb_action; loop2++ )
			dynamic_tab[loop][loop2] = 0;

	//store a first sensor model
	dynamic_tab[1][1] = 1;//P(S_t = s + 1 | S_(t-1) = s, A = 1 ) = 100%
	
	//store a second sensor model
	//dynamic_tab[1][1] = 0.95;//P(S_t = s + 1 | S_(t-1) = s, A = 1 ) = 95%
	//dynamic_tab[0][1] = 0.05;//P(S_t = s | S_(t-1) = s, A = 1 ) = 5%

	return( dynamic_tab[current_state-previous_state][action] );

}// dynamic_model

float sensor_model(int observation, int current_state) {
// return the probability to observe "observation" when we are located in the state "current_state"

	// store the map
	int loop;
	for( loop = 0; loop < nb_state; loop++ )
		map_left[loop] = wall;
		
	map_left[5] = door;
	map_left[7] = door;
	map_left[13] = door;
	
	//store a first sensor model
	sensor_tab[door][door] = 1;
	sensor_tab[wall][door] = 0;
	sensor_tab[wall][wall] = 1;
	sensor_tab[door][wall] = 0;

	//store a second sensor model
	//sensor_tab[door][door] = 0.8;
	//sensor_tab[wall][door] = 0.2;

	//sensor_tab[wall][wall] = 0.8;
	//sensor_tab[door][wall] = 0.2;
	
	return( sensor_tab[observation][map_left[current_state]] );

}// sensor_model


/**
 ** TODO next implementation of the sensor model
 **
float sensor_model(int left_observation, int right_observation, int current_state) {



}// sensor_model
*/

void prediction(int action) {

	//TODO For all remaining states, sum the posibilities of being on next step or .....
	int current_state, previous_state, i, j;

	for(j = 0; j < nb_state; j++)
	{
		//previous_localization[j] = current...
	}

	for(i = current_state; i < nb_state; i++)
	{		

	}
	
	//prediction

}// prediction

void estimation(int observation) {

	int current_state;
	
	//estimation: confrontation between prediction and observation

}// estimation

void normalization() {

	int current_state;

	//normalization
	float norm = 0;
	
	//copy of current_localization in previous_localization

}//normalization

void display() {

	int current_state;

	for( current_state = 0; current_state < nb_state; current_state++ )
		printf("P(S_t = %d) = %f\n", current_state, current_localization[current_state]);
	printf("\n");

}// display

//****************************************************
// programme principal
//****************************************************

int main(int argc, char* argv[]) {

	init_position(1);
	
	printf("prediction\n");
	prediction(1);
	normalization();
	display();
	printf("estimation\n");
	estimation(wall);
	normalization();
	display();
	
	printf("prediction\n");
	prediction(1);
	normalization();
	display();
	printf("estimation\n");
	estimation(wall);
	normalization();
	display();
	
	printf("prediction\n");
	prediction(1);
	normalization();
	display();
	printf("estimation\n");
	estimation(wall);
	normalization();
	display();
	
	printf("prediction\n");
	prediction(1);
	normalization();
	display();
	printf("estimation\n");
	estimation(wall);
	normalization();
	display();

	printf("prediction\n");
	prediction(1);
	normalization();
	display();
	printf("estimation\n");
	estimation(wall);
	normalization();
	display();

	return 0;

}// main
