#include <iostream>
/*Libraries needed*/
#include "DataReader.hpp"

int main(int argc, char* argv[]){

   std::string setName(argv[1]);
   DataReader dataObj(setName);
   int key;
   int threshold = 5; 
   
   // read the first data to determine the background
   dataObj.readData();//read the next laser data
   dataObj.initBackground();//define the background TODO
      

   // Displaying the dataset (each sliding window; each frame)

	while(dataObj.readData() == NOERROR) {
	
		dataObj.printLaserData();//display the current laser data on the graphical window

		dataObj.detectMotion(threshold); // TODO
		dataObj.printMotion();
  	dataObj.formObject(); //TODO
	}
    
     return 0;

}
