#include "Kalman.hpp"

Kalman::Kalman(float init_mean, float init_std, float init_q, float init_r) {
	mean = init_mean;
	q = init_q;
	r = init_r;
	std = init_std;

}

void Kalman::prediction(int a) {
	std = std + q;
  	mean = mean + a;
  	k = (std / (std + 1));
}

void Kalman::estimation(int o) {
	mean = mean + k;
  	std = (1-k) * std;
}

